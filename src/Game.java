import java.util.Scanner;

public class Game {

		
		private Player x;
		private Player o;
		private Board board;
		private int row ;
		private int col ;
		
		public Game() {
			x = new Player('X');
			o = new Player('O');
			board = new Board(x,o);
			row = 0;
			col = 0;
		}
		
		public void play() {
			showWelcome();
			while(true) {
				showTable();
			    showTurn();
			    input();
			}
			
			
		}
		private void showWelcome() {
			System.out.println("Welcom to OX Game");
		}
		
		private void showTable() {
			char[][] table = board.getTable();
			for(int i=0;i<4;i++)
			   {
				 for(int j=0;j<4;j++)
				 {
					 System.out.print(table[i][j]);
					 System.out.print(' ');
					
				 }
				 System.out.println();
			  }
			
		}
		
		private void showTurn() 
		{
			Player player = board.getCurrentPlayer();
			System.out.println( player.getName() +" turn");
		}
		
		private void input() {
			 Scanner kb = new Scanner(System.in);
			 System.out.println("Plase Row number = ");
		 	  row = kb.nextInt();
		     System.out.println("Plase column number = ");
			  col = kb.nextInt(); 
			 
			 board.setTable(row, col);
		
			 
			
		}



}
