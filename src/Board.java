
public class Board {
	private Player x;
	private Player o; 
	private Player currentPlayer;
	private char[][] table = {
			 {' ','1','2','3'},
				{'1','_','_','_'},
				{'2','_','_','_'},
				{'3','_','_','_'}

			
	};

	public Board(Player x,Player o) {
		this.x = x;
		this.o = o;
		currentPlayer = x;
	}
	
	public char[][] getTable() {
		return table;
	}
	
	public Player getCurrentPlayer() {
		return currentPlayer;
	}
	
	public char setTable (int row,int col) {
		if(table[row][col]=='-') {
			table[row][col] = currentPlayer.getName();
			
			return currentPlayer.getName();
		}
		return 0;
		
	}
	
}
